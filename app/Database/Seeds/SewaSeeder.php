<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SewaSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'id_titip' => 2,
            'tgl_awal'    => '2023-06-01',
            'tgl_akhir'    => '2023-06-10',
        ];

        // Using Query Builder
        $this->db->table('trans_sewa')->insertBatch($data);
    }
}
