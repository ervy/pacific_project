<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class KendaraanSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'merk' => 'Honda',
                'jenis'    => 'MVP',
                'nama'    => 'Mobilio',
                'nopol'    => 'B 1234 ABC',
            ],
            [
                'merk' => 'Mitsubishi',
                'jenis'    => 'SUV',
                'nama'    => 'Expander',
                'nopol'    => 'B 4321 ABC',
            ],
            [
                'merk' => 'Mitsubishi',
                'jenis'    => 'SUV',
                'nama'    => 'Pajero Sport',
                'nopol'    => 'B 1111 ABC',
            ]
        ];

        // Using Query Builder
        $this->db->table('m_kendaraan')->insertBatch($data);
    }
}
