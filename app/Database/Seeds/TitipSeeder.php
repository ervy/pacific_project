<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TitipSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_kendaraan' => 1,
                'tgl_titip'    => '2022-04-01',
                'harga_sewa'    => 500000,
                'tgl_berakhir'    => '',
            ], [
                'id_kendaraan' => 2,
                'tgl_titip'    => '2023-05-03',
                'harga_sewa'    => 560000,
                'tgl_berakhir'    => '',
            ],
            [
                'id_kendaraan' => 3,
                'tgl_titip'    => '2023-04-01',
                'harga_sewa'    => 1500000,
                'tgl_berakhir'    => '2023-04-30',
            ]
        ];

        // Using Query Builder
        $this->db->table('trans_titip')->insertBatch($data);
    }
}
