<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Titip extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_titip' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'id_kendaraan' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
            ],
            'tgl_titip' => [
                'type'       => 'DATE',
                'null' => 'TRUE',
            ],
            'harga_sewa' => [
                'type'       => 'INT',
                'constraint' => '11',
            ],
            'tgl_berakhir' => [
                'type'       => 'DATE',
                'null' => 'TRUE',
            ],
        ]);
        $this->forge->addKey('id_titip', true);
        $this->forge->addForeignKey('id_kendaraan', 'm_kendaraan', 'id_kendaraan', 'CASCADE', 'CASCADE');
        $this->forge->createTable('trans_titip');
    }

    public function down()
    {
        $this->forge->dropTable('trans_titip');
    }
}
