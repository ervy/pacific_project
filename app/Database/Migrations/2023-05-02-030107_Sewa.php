<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Sewa extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_sewa' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'id_titip' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
            ],
            'tgl_awal' => [
                'type'       => 'DATE',
                'null' => 'TRUE',
            ],
            'tgl_akhir' => [
                'type'       => 'DATE',
                'null' => 'TRUE',
            ],
        ]);
        $this->forge->addKey('id_sewa', true);
        $this->forge->addForeignKey('id_titip', 'trans_titip', 'id_titip', 'CASCADE', 'CASCADE');
        $this->forge->createTable('trans_sewa');
    }

    public function down()
    {
        $this->forge->dropTable('trans_sewa');
    }
}
