<?php

namespace App\Controllers;

use App\Models\Kendaraan;
use App\Models\Penitipan;
use App\Models\Penyewaan;
use App\Controllers\BaseController;

class PagesController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'home'
        ];

        return view('pages/home', $data);
    }

    public function kendaraan()
    {
        $kendaraanModel = new Kendaraan();
        $kendaraan = $kendaraanModel->findAll();
        $data = [
            'title' => 'kendaraan',
            'kendaraan' => $kendaraan
        ];

        return view('pages/kendaraan', $data);
    }

    public function penitipan()
    {
        $penitipanModel = new Penitipan();
        $penitipan = $penitipanModel->findAll();
        $kendaraanModel = new Kendaraan();
        $kendaraan = $kendaraanModel->findAll();
        $penyewaanModel = new Penyewaan();
        $penyewaan = $penyewaanModel->findAll();
        $data = [
            'title' => 'penitipan',
            'penitipan' => $penitipan,
            'kendaraan' => $kendaraan,
            'penyewaan' => $penyewaan
        ];

        return view('pages/penitipan', $data);
    }

    public function penyewaan()
    {
        $penyewaanModel = new Penyewaan();
        $penyewaan = $penyewaanModel->findAll();
        $penitipanModel = new Penitipan();
        $penitipan = $penitipanModel->findAll();
        $kendaraanModel = new Kendaraan();
        $kendaraan = $kendaraanModel->findAll();
        $data = [
            'title' => 'penyewaan',
            'penyewaan' => $penyewaan,
            'penitipan' => $penitipan,
            'kendaraan' => $kendaraan
        ];

        return view('pages/penyewaan', $data);
    }
}
