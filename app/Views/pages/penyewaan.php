<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">

            <table class="table table-hover mt-4">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Merk</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Awal Sewa</th>
                        <th scope="col">Tanggal Berakhir Sewa</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($penyewaan as $py) : ?>
                        <?php foreach ($penitipan as $pt) : ?>
                            <?php foreach ($kendaraan as $k) : ?>
                                <?php if ($pt['id_titip'] == $py['id_titip']) : ?>
                                    <?php if ($k['id_kendaraan'] == $pt['id_kendaraan']) : ?>
                                        <tr>
                                            <th scope="row"><?= $i++; ?></th>
                                            <td><?= $k['merk']; ?></td>
                                            <td><?= $k['nama']; ?></td>
                                            <td><?= $py['tgl_awal']; ?></td>
                                            <td><?= $py['tgl_akhir']; ?></td>
                                            <td>
                                            <button type='button' class='btn btn-warning'>Sedang Disewakan</button>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<?= $this->endSection(); ?>