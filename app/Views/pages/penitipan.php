<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">

            <table class="table table-hover mt-4">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Merk</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Titip</th>
                        <th scope="col">Harga Sewa</th>
                        <th scope="col">Tanggal Berakhir</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($penitipan as $pt) : ?>
                        <?php foreach ($kendaraan as $k) : ?>
                            <?php foreach ($penyewaan as $py) : ?>
                                <?php if ($pt['id_kendaraan'] == $k['id_kendaraan']) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++; ?></th>
                                        <td><?= $k['merk']; ?></td>
                                        <td><?= $k['nama']; ?></td>
                                        <td><?= $pt['tgl_titip']; ?></td>
                                        <td><?= $pt['harga_sewa']; ?></td>
                                        <td><?= $pt['tgl_berakhir']; ?></td>
                                        <td>
                                            <?php
                                            $hasil = "Dititipkan";
                                            if ($pt['tgl_berakhir'] != '0000-00-00') {
                                                $hasil = "Penitipan berakhir";
                                            }
                                            ?>
                                            <p><?= $hasil; ?></p>
                                        </td>
                                        <td>
                                            <?php
                                            if ($pt['tgl_berakhir'] != '0000-00-00') {
                                                $hasil = "Tidak Ready";
                                                echo "<button type='button' class='btn btn-danger'>$hasil</button>";
                                            } else if ($pt['id_titip'] != $py['id_titip']) {
                                                $hasil = "Sewa Sekarang";
                                                echo "<button type='button' class='btn btn-primary'>$hasil</button>";
                                            } else {
                                                $hasil = "Sudah disewa";
                                                echo "<button type='button' class='btn btn-warning'>$hasil</button>";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<?= $this->endSection(); ?>