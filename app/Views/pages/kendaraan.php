<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">

            <table class="table table-hover mt-4">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Merk</th>
                        <th scope="col">Jenis</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Nopol</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($kendaraan as $k) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td><?= $k['merk']; ?></td>
                            <td><?= $k['jenis']; ?></td>
                            <td><?= $k['nama']; ?></td>
                            <td><?= $k['nopol']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<?= $this->endSection(); ?>