<?php

namespace App\Models;

use CodeIgniter\Model;

class Penyewaan extends Model
{
    protected $table = 'trans_sewa';
    protected $useTimestamps = true;
}
