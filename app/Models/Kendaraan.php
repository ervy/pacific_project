<?php

namespace App\Models;

use CodeIgniter\Model;

class Kendaraan extends Model
{
    protected $table = 'm_kendaraan';
    protected $useTimestamps = true;
}
